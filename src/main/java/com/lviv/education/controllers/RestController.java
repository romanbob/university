package com.lviv.education.controllers;

import com.lviv.education.models.Group;
import com.lviv.education.models.Student;
import com.lviv.education.repositories.GroupRepository;
import com.lviv.education.repositories.StudentRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import org.hibernate.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/rest")
public class RestController {
    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentRepository studentRepository;

    private final static Logger logger = LoggerFactory.getLogger(RestController.class);

    /* GROUPS */
    @ApiOperation(value = "Gets group by id")
    @ApiResponse(code=200, message = "Info about group")
    @RequestMapping(value="group/{id}", method= RequestMethod.GET, produces = "application/json")
    public @ResponseBody Group getGroupById(@PathVariable @ApiParam(value="Group id", required = true) long id) {
        return groupRepository.getGroupById(id);
    }

    @RequestMapping(value="group", method= RequestMethod.GET)
    public @ResponseBody List<Group> getAllGroups() {
        return groupRepository.getAllGroups();
    }

    @RequestMapping(value="group", method= RequestMethod.POST)
    public @ResponseBody void addGroup(@RequestBody Group group) {
        groupRepository.addGroup(group);
    }

    @RequestMapping(value="group", method= RequestMethod.PUT)
    public @ResponseBody void updateGroup(@RequestBody Group group) {
        groupRepository.updateGroup(group);
    }

    @RequestMapping(value="group", method= RequestMethod.DELETE)
    public @ResponseBody void deleteGroup(@RequestBody Group group) {
        groupRepository.deleteGroup(group);
    }

    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    /* STUDENTS */
    @RequestMapping(value="student", method= RequestMethod.POST)
    public @ResponseBody Student addStudent(@RequestBody Student student) {
        return studentRepository.addStudent(student);
    }

    @RequestMapping(value="student/{id}", method= RequestMethod.GET)
    public @ResponseBody Student getStudentById(@PathVariable long id) {
        return studentRepository.getStudentById(id);
    }

    @RequestMapping(value="student/group/{groupId}", method= RequestMethod.GET)
    public @ResponseBody List<Student> getAllStudentsByGroupId(@PathVariable long groupId) {
        return studentRepository.getAllStudentsByGroup(groupId);
    }

    @RequestMapping(value="student", method= RequestMethod.GET)
    public @ResponseBody List<Student> getAllStudents() {
        return studentRepository.getAllStudents();
    }

    @RequestMapping(value="student", method= RequestMethod.PUT)
    public @ResponseBody void updateStudent(@RequestBody Student student) {
        studentRepository.updateStudent(student);
    }

    @RequestMapping(value="student", method= RequestMethod.DELETE)
    public @ResponseBody void deleteStudent(@RequestBody Student student) {
        studentRepository.deleteStudent(student);
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }

    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    // Connection pool
    @RequestMapping(value = "statistics", method = RequestMethod.GET)
    public @ResponseBody void statistics(){
        logger.info("begin of the statistics  method");
        Statistics statistics = this.studentRepository.getSessionFactory().getStatistics();
        System.out.printf("is Enabled: %n" + statistics.isStatisticsEnabled());
        statistics.logSummary();
    }
}
