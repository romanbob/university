package com.lviv.education.repositories;

import com.lviv.education.models.Group;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class GroupRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public GroupRepository() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addGroup(Group group) {
        getCurrentSession().persist(group);
    }

    public void updateGroup(Group group) {
        getCurrentSession().update(group);
    }

    public void deleteGroup(Group group) {
        getCurrentSession().delete(group);
    }

    public Group getGroupById(long id) {
        return (Group) getCurrentSession().get(Group.class, id);
    }

    public List<Group> getAllGroups() {
        Query query = getCurrentSession().createQuery("from Group");
        return (List<Group>) query.list();
    }
}
