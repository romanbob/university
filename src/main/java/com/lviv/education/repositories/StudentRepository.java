package com.lviv.education.repositories;

import com.lviv.education.models.Student;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class StudentRepository {
    @Autowired
    private SessionFactory sessionFactory;

    public StudentRepository() {
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Student addStudent(Student student) {
        // 'persist' in order to be able to make cascading saving
        getCurrentSession().persist(student);
        return student;
    }

    public void updateStudent(Student student) {
        getCurrentSession().update(student);
    }

    public void deleteStudent(Student student) {
        getCurrentSession().delete(student);
    }

    public Student getStudentById(long id) {
        return (Student) getCurrentSession().get(Student.class, id);
    }

    public List<Student> getAllStudents() {
        /*HQL*/
        Query query = getCurrentSession().createQuery("from Student");
        return (List<Student>) query.list();
    }

    public List<Student> getAllStudentsByGroup(long groupId) {
        SQLQuery query = getCurrentSession().createSQLQuery("select * from students where group_id=:groupId");
        query.addEntity(Student.class);
        query.setParameter("groupId", groupId);
        return (List<Student>) query.list();
    }
}
