###Configure MYSQL on MAC OS###
- installation

`brew install mysql`
- start server as service:

`brew services start mysql`

- start server:

`mysql.server start`

- work with console tool:

`mysql -uroot`

- to turn off auth_socket plugin
 
`ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'your_new_password';`

### Hibernate ###
- Infinite Recursion

http://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion

### Swagger ###
Article how to config: https://www.concretepage.com/spring-4/spring-rest-swagger-2-integration-with-annotation-xml-example
* JSON response of API documentation
http://localhost:3000/university/api/v2/api-docs

* Swagger UI
http://localhost:3000/university/api/swagger-ui.html

### Logging ###
#### Logging Dependencies in Spring ####
* Logging Dependencies in Spring
https://spring.io/blog/2009/12/04/logging-dependencies-in-spring/

#### Hibernate Logging ####
* Best Practice
https://stackify.com/9-logging-sins-java/
* Using Log4j2 with Spring
https://springframework.guru/category/java/log4j2/
* Hibernate Logging Guide
https://www.thoughts-on-java.org/hibernate-logging-guide/
* JBoss Logging library
http://docs.jboss.org/hibernate/orm/4.3/topical/html/logging/Logging